This project is released under the terms of the MIT license, which is contained
in the file "LICENSES/MIT.txt".

All copyright and licensing information for this project is provided in a
machine-readable format in the file ".reuse/dep5".  This file is described in
the REUSE Specification:

  https://reuse.software/spec/
