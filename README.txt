This program displays graphemes (including grapheme clusters) in UTF-8 text.

An example:

$ env printf '\xc3\xa9\x65\xcc\x81' | ./unicode-graphemes.py
U+00E9 233 LATIN SMALL LETTER E WITH ACUTE
Grapheme cluster
	U+0065 101 LATIN SMALL LETTER E
	U+0301 769 COMBINING ACUTE ACCENT
$ env printf '\xc3\xa9\x65\xcc\x81' | ./unicode-graphemes.py --show-glyphs
U+00E9 233 LATIN SMALL LETTER E WITH ACUTE: é
Grapheme cluster: é
	U+0065 101 LATIN SMALL LETTER E: e
	U+0301 769 COMBINING ACUTE ACCENT: ́

This program requires Python 3 and PyICU. On a Debian or Ubuntu system, you can
probably install PyICU with this command:

  apt install python3-icu

You can also get PyICU from PyPI: https://pypi.org/project/PyICU/

Once you have PyICU installed, you can run unicode-graphemes-for-python.py on
any UTF-8 text file:

  ./unicode-graphemes-for-python.py FILE.txt

By default, the output contains only Unicode code points (in hexadecimal and
decimal) and character names. You can use the --show-glyphs option to include
the actual glyphs in the output. (Your mileage may vary depending on what
terminal you are using, what fonts you have installed, etc.)

  ./unicode-graphemes-for-python.py --show-glyphs FILE.txt

This program is free software. See the file "LICENSE.txt" for license terms.
