#!/usr/bin/env python3

import argparse
import curses.ascii
import sys
import unicodedata

import icu

def format_code_point(code_point):
  return 'U+{:04X}'.format(code_point)

def string_contains_control_character(s):
  for c in s:
    if curses.ascii.iscntrl(c):
      return True
  return False

PROGRAM_NAME = 'unicode-graphemes.py'

def main():
  parser = argparse.ArgumentParser(formatter_class = argparse.RawDescriptionHelpFormatter, description = 'Display graphemes in file (must be UTF-8).\n\nWith no file, or when file is -, read standard input.')
  parser.add_argument('file', nargs = '?', type = argparse.FileType('rb'), default = sys.stdin.buffer)
  parser.add_argument('--show-glyphs', help = '(attempt to) display glyphs', action = 'store_true')
  args = parser.parse_args()

  try:
    locale = icu.Locale.getUS()
    break_iterator = icu.BreakIterator.createCharacterInstance(locale)

    # read the file one line at a time
    for line in args.file:
      # convert from bytes to str
      line = line.decode()

      # convert from str to UTF-16
      # NOTE: we need to specify little-endian explicitly because without it Python will add a BOM
      utf_16 = line.encode('utf_16_le')

      break_iterator.setText(line)
      grapheme_cluster_start_boundary = break_iterator.first()
      for grapheme_cluster_end_boundary in break_iterator:
        indent = False

        grapheme_cluster = utf_16[2*grapheme_cluster_start_boundary:2*grapheme_cluster_end_boundary]
        grapheme_cluster = grapheme_cluster.decode('utf_16_le')

        if len(grapheme_cluster) > 1:
          # this is a grapheme cluster with multiple code points - print out the grapheme cluster
          sys.stdout.write('Grapheme cluster')
          if args.show_glyphs:
            sys.stdout.write(': ')
            if not string_contains_control_character(grapheme_cluster):
              sys.stdout.write(grapheme_cluster)
          sys.stdout.write('\n')
          indent = True

        for c in grapheme_cluster:
          code_point = ord(c)

          # get the character name
          character_name = unicodedata.name(c, '')

          # print out the code point
          if indent:
            sys.stdout.write('\t')
          sys.stdout.write(format_code_point(code_point))
          sys.stdout.write(' ')
          sys.stdout.write(str(code_point))
          sys.stdout.write(' ')
          sys.stdout.write(character_name)
          if args.show_glyphs:
            sys.stdout.write(': ')
            if not curses.ascii.iscntrl(c):
              sys.stdout.write(c)
          sys.stdout.write('\n')

        grapheme_cluster_start_boundary = grapheme_cluster_end_boundary
  except Exception as e:
    sys.stderr.write('{0}: {1}\n'.format(PROGRAM_NAME, e))

if __name__ == '__main__':
  main()
